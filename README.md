# mpd2ldr.html - Introduction

mpd2ldr.html is a standalone HTML/ECMAScript application to flatten LDraw
models.  It transforms multi-model files (.MPD) to flat/unique-model files
(.LDR).

By “standalone application” we mean that the user just needs to open the file
mpd2ldr.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.


mpd2ldr.html is licenced under GPLv3+.


# Requirements

* NONE!


# Usage

Just copy the files on your computer and open mpd2ldr.html in a web browser.
Select the files to transform and then download the results.
