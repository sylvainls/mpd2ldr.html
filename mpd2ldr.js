/*
 *  Copyright 2019, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

// Math stuff
function mat_mul( m, n ) {
    let r =  [ [ 1, 0, 0, 0 ],
               [ 0, 1, 0, 0 ],
               [ 0, 0, 1, 0 ],
               [ 0, 0, 0, 1] ];
    if ( !m ) {
        if ( !n ) {
            return r;
        } else {
            return n;
        }
    } else if ( !n ) {
        return m;
    }
    r[0][0] = m[0][0] * n[0][0] + m[1][0] * n[0][1] + m[2][0] * n[0][2] + m[3][0] * n[0][3];
    r[1][0] = m[0][0] * n[1][0] + m[1][0] * n[1][1] + m[2][0] * n[1][2] + m[3][0] * n[1][3];
    r[2][0] = m[0][0] * n[2][0] + m[1][0] * n[2][1] + m[2][0] * n[2][2] + m[3][0] * n[2][3];
    r[3][0] = m[0][0] * n[3][0] + m[1][0] * n[3][1] + m[2][0] * n[3][2] + m[3][0] * n[3][3];

    r[0][1] = m[0][1] * n[0][0] + m[1][1] * n[0][1] + m[2][1] * n[0][2] + m[3][1] * n[0][3];
    r[1][1] = m[0][1] * n[1][0] + m[1][1] * n[1][1] + m[2][1] * n[1][2] + m[3][1] * n[1][3];
    r[2][1] = m[0][1] * n[2][0] + m[1][1] * n[2][1] + m[2][1] * n[2][2] + m[3][1] * n[2][3];
    r[3][1] = m[0][1] * n[3][0] + m[1][1] * n[3][1] + m[2][1] * n[3][2] + m[3][1] * n[3][3];

    r[0][2] = m[0][2] * n[0][0] + m[1][2] * n[0][1] + m[2][2] * n[0][2] + m[3][2] * n[0][3];
    r[1][2] = m[0][2] * n[1][0] + m[1][2] * n[1][1] + m[2][2] * n[1][2] + m[3][2] * n[1][3];
    r[2][2] = m[0][2] * n[2][0] + m[1][2] * n[2][1] + m[2][2] * n[2][2] + m[3][2] * n[2][3];
    r[3][2] = m[0][2] * n[3][0] + m[1][2] * n[3][1] + m[2][2] * n[3][2] + m[3][2] * n[3][3];

    r[0][3] = m[0][3] * n[0][0] + m[1][3] * n[0][1] + m[2][3] * n[0][2] + m[3][3] * n[0][3];
    r[1][3] = m[0][3] * n[1][0] + m[1][3] * n[1][1] + m[2][3] * n[1][2] + m[3][3] * n[1][3];
    r[2][3] = m[0][3] * n[2][0] + m[1][3] * n[2][1] + m[2][3] * n[2][2] + m[3][3] * n[2][3];
    r[3][3] = m[0][3] * n[3][0] + m[1][3] * n[3][1] + m[2][3] * n[3][2] + m[3][3] * n[3][3];

    return r;
}

function vec_add( u, v ) {
    if ( !u ) {
        if ( !v ) {
            return [ 0, 0, 0 ];
        } else {
            return v;
        }
    } else if ( !v ) {
        return u;
    }
    return [ u[0] + v[0], u[1] + v[1], u[2] + v[2] ];
}

function vec_mul_scal( v, s ) {
    return [ v[0] * s, v[1] * s, v[2] * s ];
}

function mat_mul_vec( m, v ) {
    let x, y, z, w;
    x = v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0] + m[3][0];
    y = v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1] + m[3][1];
    z = v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2] + m[3][2];
    w = v[0] * m[0][3] + v[1] * m[1][3] + v[2] * m[2][3] + m[3][3];
    if ( w == 1.0 ) {
        return [ x, y, z ];
    } else {
        return [ x / w, y / w, z / w ];
    }
}

const M4ID = [ [ 1, 0, 0, 0 ],
               [ 0, 1, 0, 0 ],
               [ 0, 0, 1, 0 ],
               [ 0, 0, 0, 1] ];

function read_matrix( str ) {
    let r = str.split( /\s+/ ).map( function( f ) { return +f; } );
    return [ [ r[3],  r[4],  r[5], r[0] ],
             [ r[6],  r[7],  r[8], r[1] ],
             [ r[9], r[10], r[11], r[2] ],
             [    0,     0,     0,    1 ] ];
}

function read_vectors( str ) {
    let elts = str.split( /\s+/ );
    let vs = []
    for ( let i = 0; i < elts.length / 3; i++ ) {
        vs.push( [ elts[i*3], elts[i*3+1], elts[i*3+2] ] );
    }
    return vs;
}

const PRECISION = 5;

function vectors_to_s( vs ) {
    return vs.map( function( v ) {
        return v.map ( function( x ) {
            return x.toFixed( PRECISION );
        } ).join( " " );
    } ).join( " " );
}

function matrix_to_s( m ) {
    return [
        m[0][3], m[1][3], m[2][3],
        m[0][0], m[0][1], m[0][2],
        m[1][0], m[1][1], m[1][2],
        m[2][0], m[2][1], m[2][2]
    ].map( function( f ) { return f.toFixed( PRECISION ); } ).join( " " );
}

const NEW_MODEL_RE = /^0\s+FILE\s+(.*)/;
const END_MODEL_RE = /^0\s+NOFILE\s*/;
const COMMENT_RE = /^\s*0(.*)/;
const USAGE_RE = /^\s*1\s+(\d+)\s+(([^\s]+\s+){12})(.*)/;
const OTHER_RE = /^\s*([2-5])\s+(\d+)\s+(.*)/;

const LINE_TYPE = { "Empty": -1, "Comment": 0, "Use": 1 };

function pout( model, color, matrix, models, output ) {
    for ( let line of model.lines ) {
        switch ( line.type ) {
        case LINE_TYPE.Empty:
            output.push( "" );
            break;
        case LINE_TYPE.Comment:
            output.push( "0 " + line.txt );
            break;
        case LINE_TYPE.Use:
            let actual_color = line.color == 16 ? color : line.color;
            let actual_matrix = mat_mul( line.matrix, matrix );
            if ( models[line.file] ) {
                output.push( "0 // " + line.file + " in " + line.color + " at "
                             + matrix_to_s( line.matrix ) );
                pout( models[line.file], actual_color, actual_matrix, models, output );
            } else {
                output.push( "1 " + actual_color + " " + matrix_to_s(actual_matrix)
                             + " " + line.file );
            }
            break;
        default:
            if ( !line.vectors ) {
                ; // Error!
            }
            let actual_color_ = line.color == 16 ? color : line.color;
            let actual_vectors = line.vectors.map( function( v ) {
                return mat_mul_vec( matrix, v );
            } );
            output.push( line.type + " " + actual_color_ + " "
                         + vectors_to_s( actual_vectors ) );
            break;
        }
    }
}

function mpd2ldr( lines, finished_cb ) {
    let models = {};
    let main_model = null;
    let cur_model = null;
    let intro = [];
    let line_nr = 0;

    // read everything before the first/main model
    while ( line_nr < lines.length ) {
        let line = lines[line_nr++].replace( /(\n|\r)+$/, "" );
        if ( NEW_MODEL_RE.test( line ) ) {
            let match = NEW_MODEL_RE.exec( line );
            cur_model = {
                file: match[1],
                lines: []
            };
            models[ cur_model.file ] = cur_model;
            main_model = cur_model;
            break;
        } else {
            intro.push( line );
        }
    }
    // read the model
    while ( line_nr < lines.length ) {
        let line = lines[line_nr++].replace( /(\n|\r)+$/, "" );
        if ( NEW_MODEL_RE.test( line ) ) {
            let match = NEW_MODEL_RE.exec( line );
            cur_model = {
                file: match[1],
                lines: []
            };
            models[ cur_model.file ] = cur_model;
        } else if ( END_MODEL_RE.test( line ) ) {
            ; // nothing
        } else if ( line == "" ) {
            cur_model.lines.push( {
                type: LINE_TYPE.Empty
            } );
        } else if ( COMMENT_RE.test( line ) ) {
            let match = COMMENT_RE.exec( line );
            cur_model.lines.push( {
                type: LINE_TYPE.Comment,
                txt: match[1]
            } );
        } else if ( USAGE_RE.test( line ) ) {
            let match = USAGE_RE.exec( line );
            cur_model.lines.push( {
                type: LINE_TYPE.Use,
                color: match[1],
                matrix: read_matrix( match[2] ),
                file: match[4]
            } );
        } else if ( OTHER_RE.test( line ) ) {
            let match = OTHER_RE.exec( line );
            cur_model.lines.push( {
                type: match[1],
                color: match[2],
                vectors: read_vectors( match[3] )
            } );
        } else {
            ; // Error actually but we just ignore them
        }
    }

    // convert
    let output = [];
    if ( main_model ) {
        // rewrite the intro as comments
        for ( let com of intro ) {
            output.push( "0 " + com );
        }
        // convert the main model
        pout( main_model, 16, M4ID, models, output );
    } else {
        // not an MPD, leave it alone
        output = intro;
    }

    // all done
    finished_cb( output.join( "\r\n" ) );
}

// main conversion function
function mpd2ldr_convert( file, finished_cb ) {

    // read the file
    let reader = new FileReader();
    let lines = [];
    reader.onload = function( progress ) {
        lines = this.result.split( "\n" );
        mpd2ldr( lines, finished_cb );
    };
    reader.readAsText( file );
}
